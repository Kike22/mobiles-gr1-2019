interface IAnimal {
    fun name(): String
    fun family(): String
}

class Snake: IAnimal{
  override fun name():String{
      return "serpiente"
  }

    override fun family(): String {
        return "reptil"
    }
}
class SeaDog: IAnimal{
    override fun name(): String {
        return "foca"
    }

    override fun family(): String {
        return "mamífero"
    }

}

class Condor: IAnimal  {
    override fun name(): String {
        return "condor"
    }

    override fun family(): String {
        return "ave"
    }
}

enum class Province(){
    Pichincha, Sucumbios, Pastaza, Galapagos

}

fun animal(province: Province): IAnimal? {
    when(province){
        Province.Pichincha -> return Condor()
        Province.Pastaza, Province.Sucumbios -> return Snake()
        Province.Galapagos -> return SeaDog()
        else -> return null
    }
}

fun main (args: Array<String>){
    val animalSinFamilia = "\"Animal disponible por provincia\""

    println(animal(Province.Sucumbios)?.name())
    println(animal(Province.Galapagos)?.name())
    println(animal(Province.Pastaza)?.name())
    println(animal(Province.Pichincha)?.name())


}



