package com.example.time_fight

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var welcomeMessageTextView: TextView // lateinit sirve para que el elemento no utilice memoria hasta que en verdad sean utilizados o se cargue un valor
    private lateinit var welcomeSubtitleTextView: TextView
    private lateinit var changeTextButton: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        welcomeMessageTextView = findViewById(R.id.welcome_message) //busca en los recursosos R cualquier tipo, en este caso ID's id.nombre_del_id
        welcomeSubtitleTextView = findViewById(R.id.welcome_subtitle)
        changeTextButton = findViewById(R.id.change_text_button)

        changeTextButton.setOnClickListener { changesMesaggesAndSubtitles() } // estara esuchando hasta q den clic en el boton y ejecute el metodo creado -> changesMesagesAndSubtitles

    }

    private fun changesMesaggesAndSubtitles(){
        welcomeMessageTextView.text = getText(R.string.new_welcome_message)
        welcomeSubtitleTextView.text = getText(R.string.new_welcome_subtitle)



    }
}
